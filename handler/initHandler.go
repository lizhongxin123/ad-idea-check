package handler

import (
	"ad-idea-check/server"
	"ad-idea-check/share"
	"fmt"
	"time"
)

type CheckHandler interface {
	Init()
	Run() error
}

var checkHandlerServer []CheckHandler

func InitCheckHandler() {
	newIdeaCheckServer := server.NewIdeaCheckServer()
	newIdeaCheckServer.Init()

	checkHandlerServer = append(checkHandlerServer, newIdeaCheckServer)
}

func CheckHandlerServer() {
	for {
		fmt.Println("Do Run CheckHandlerServer:", time.Now().String())
		for _, server := range checkHandlerServer {
			server.Run()
		}

		time.Sleep(share.GlobalContext.Server.TaskIntervalDuration * time.Second)
		fmt.Println()
	}
}
