package model

type Idea struct {
	Id           int    `json:"id" xorm:"id" comment:""`
	PlanId       int    `json:"plan_id" xorm:"plan_id" comment:""`
	UserID       int    `json:"user_id" xorm:"user_id" comment:""`
	Status       int    `json:"status" xorm:"status" comment:""`
	Name         string `json:"name" xorm:"name" comment:""`
	Image        string `json:"image" xorm:"image" comment:""`
	Audit        int    `json:"audit" xorm:"audit" comment:""`
	RefuseReason string `json:"refuse_reason" xorm:"refuse_reason" comment:""`
	VideoId      string `json:"video_id" xorm:"video_id" comment:""`
	RefuseType   string `json:"refuse_type" xorm:"refuse_type" comment:""`
}
