package model

import "time"

type Resource struct {
	Id            int       `json:"id" xorm:"id" comment:""`
	Name          string    `json:"name" xorm:"name" comment:""`
	FileName      string    `json:"file_name" xorm:"file_name" comment:""`
	RemoteUrl     string    `json:"remote_url" xorm:"remote_url" comment:""`
	Status        string    `json:"status" xorm:"status" comment:""`
	RefuseReason  string    `json:"refuse_reason" xorm:"refuse_reason" comment:""`
	AuditTime     time.Time `json:"audit_time" xorm:"audit_time" comment:""`
	AuditUserName string    `json:"audit_user_name" xorm:"audit_user_name" comment:""`
}
