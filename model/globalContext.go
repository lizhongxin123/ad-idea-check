package model

import "time"

type GlobalContext struct {
	Server struct {
		TaskIntervalDuration time.Duration
		ServerReadTimeOut    time.Duration
	}

	Adv struct {
		DbType             string
		DbUser             string
		DbPassword         string
		DbHost             string
		DbName             string
		SetConnMaxLifetime int
		DbMaxIdleConn      int
		DbMaxOpenConn      int
	}

	Log struct {
		Name           string
		LogPath        string
		BufSize        int
		MaxEnableLevel int
		NeedEmail      bool
	}

	Email struct {
		Server    string
		Sender    string
		Password  string
		Receivers []string
		Subject   string
	}
}
