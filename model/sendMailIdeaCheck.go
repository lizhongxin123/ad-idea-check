package model

type SendMailIdea struct {
	IdeaId    int
	PlanId    int
	UserId    int
	Name      string
	Resources []Resource
}
