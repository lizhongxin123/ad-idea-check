package main

import (
	"ad-idea-check/handler"
	"ad-idea-check/share"
	"ad-idea-check/share/runtime"
	"time"
)

func init() {
	// init share
	share.InitShare()

	// init handler
	handler.InitCheckHandler()
}

func main() {
	t1 := time.Now()
	runtime.CLog.Debug("main", "开始任务：%s", t1.String())

	handler.CheckHandlerServer()
	t2 := time.Now()

	runtime.CLog.Debug("main", "结束任务：%s ,耗时：%d ", t2.String(), t2.Sub(t1))
}
