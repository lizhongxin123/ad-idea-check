package server

import (
	"ad-idea-check/dao"
	"ad-idea-check/model"
	"ad-idea-check/share"
	"ad-idea-check/share/runtime"
	utils "ad-idea-check/util"
	"bytes"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/smtp"
	"strings"
	"time"
)

var (
	ideaCheckServerName = "idea_check"
	limitLen            = 20

	ideaMap        map[int]model.Idea       //存放待检查数据
	checkResultMap map[int][]model.Resource //存放检查结果数据

	statusBegin = 0
	auditBegin  = 1

	ideaStatusAfter = -1
	ideaAuditAfter  = 2

	resourceStatus = "2"
)

type IdeaCheckServer struct {
	ideaDao     *dao.IdeaDao
	resourceDao *dao.ResourceDao
	name        string
}

func NewIdeaCheckServer() *IdeaCheckServer {
	return &IdeaCheckServer{
		ideaDao:     dao.NewIdeaDao(),
		resourceDao: dao.NewResourceDao(),
	}
}

func (s *IdeaCheckServer) Init() {
	s.name = ideaCheckServerName

}

func (s *IdeaCheckServer) Run() error {
	if s.name != ideaCheckServerName {
		errs := fmt.Sprintf("this server [%s] is on't match it [%s]", s.name, ideaCheckServerName)
		runtime.CLog.Debug("Run", errs)
		return errors.New(errs)
	}

	ideaMap = make(map[int]model.Idea, 0)              //存放待检查数据
	checkResultMap = make(map[int][]model.Resource, 0) //存放检查结果数据

	//获取全部正在运行的创意id
	ideaList, err := s.ideaDao.FindDataListByStatusAndAudit(statusBegin, auditBegin)
	if err != nil {
		runtime.CLog.Debug("Run", "FindDataListByStatusAndAudit err : %v", err)
		return err
	}
	for _, idea := range ideaList {
		ideaMap[idea.Id] = idea
	}
	if len(ideaMap) <= 0 {
		runtime.CLog.Debug("Run", "暂无上线的广告创意。。。")
		fmt.Println("暂无上线的广告创意。。。")
		return nil
	}

	// 处理每一个创意
	var count int
	ideaIdList := make([]int, 0)
	for i := 0; i < len(ideaList); i++ {
		ideaIdList = append(ideaIdList, ideaList[i].Id)
		count += 1
		if count >= limitLen {
			// 检查创意下的素材
			s.checkResource(ideaIdList)
			count = 0
			ideaIdList = make([]int, 0)
		}
	}
	s.checkResource(ideaIdList)

	// 修正每一个创意和创意下的所有素材
	if len(checkResultMap) > 0 {
		s.updateDataByResult()
		noticeDutyOfficer()
	}

	return nil
}

// checkResource 检查创意下的素材
func (s *IdeaCheckServer) checkResource(ideaIdList []int) {
	for _, ideaId := range ideaIdList {
		idea := ideaMap[ideaId]
		imageId := strings.Split(idea.Image, ",")
		videoId := strings.Split(idea.VideoId, ",")

		imageId = append(imageId, videoId...)
		resourceList, err := s.resourceDao.FindDataListByIds(imageId...)
		if err != nil {
			runtime.CLog.Debug("checkResource", "FindDataListByIds err : %v", err)
			return
		}

		checkOssUrlResult := make([]model.Resource, 0)
		for _, resource := range resourceList {
			if !checkOssUrl(resource.RemoteUrl) {
				checkOssUrlResult = append(checkOssUrlResult, resource)
			}
		}

		if len(checkOssUrlResult) > 0 {
			checkResultMap[ideaId] = checkOssUrlResult
		}

	}
}

// updateDataByResult 修正创意和创意下的所有素材
func (s *IdeaCheckServer) updateDataByResult() {
	ideaIdList := make([]int, 0)
	for idea, resourceList := range checkResultMap {
		ideaIdList = append(ideaIdList, idea)
		resourceId := make([]int, 0)
		for _, resource := range resourceList {
			resourceId = append(resourceId, resource.Id)
		}

		err := s.resourceDao.UpdateStatusByIds(resourceStatus, "素材被封禁", "管理员", time.Now(), resourceId...)
		if err != nil {
			runtime.CLog.Debug("updateDataByResult", "UpdateStatusByIds err : %v", err)
			return
		}
	}

	err := s.ideaDao.UpdateStatusByIds(ideaStatusAfter, ideaAuditAfter, "4", ideaIdList...)
	if err != nil {
		runtime.CLog.Debug("updateDataByResult", "UpdateStatusByIds err : %v", err)
		return
	}
}

// checkOssUrl 校验ossUrl
func checkOssUrl(url string) bool {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Println(err)
	}
	req.Header.Add("Cookie", "advanced-frontend=2c2700455c60bee79495b17938177227")

	client := &http.Client{}
	res, err := client.Do(req)
	if err != nil {
		runtime.CLog.Debug("checkOssUrl", "client.Do err : %v", err)
		return false
	}
	defer res.Body.Close()

	if res.StatusCode == http.StatusForbidden {
		return false
	}

	return true
}

// noticeDutyOfficer 邮件通知责任人
func noticeDutyOfficer() {
	noticeList := make([]model.SendMailIdea, 0)
	for ideaId, resourceList := range checkResultMap {
		var notice model.SendMailIdea
		if idea, ok := ideaMap[ideaId]; ok {
			notice.IdeaId = idea.Id
			notice.PlanId = idea.PlanId
			notice.UserId = idea.UserID
			notice.Name = idea.Name
			notice.Resources = resourceList
		}
		noticeList = append(noticeList, notice)
	}

	data, err := utils.Encode(noticeList)
	if err != nil {
		log.Println(err)
	}

	if err := SendMail(string(data)); err != nil {
		runtime.CLog.Debug("noticeDutyOfficer", "SendMail err : %v", err)
		log.Println("err", err)
	}
}

func SendMail(content string) (err error) {
	var data bytes.Buffer
	_, _ = fmt.Fprintf(&data, "To: %s\r\n", strings.Join(share.GlobalContext.Email.Receivers, ","))
	_, _ = fmt.Fprintf(&data, "From: %s\r\n", share.GlobalContext.Email.Sender)
	_, _ = fmt.Fprintf(&data, "Subject: %s\r\n", share.GlobalContext.Email.Subject)
	_, _ = fmt.Fprintf(&data, "Content-Type: text/plain; charset=UTF-8\r\n")
	_, _ = fmt.Fprintf(&data, "\r\n%s", share.GlobalContext.Email.Server)
	_, _ = fmt.Fprintf(&data, "\r\n%s", content)

	hp := strings.Split(share.GlobalContext.Email.Server, ":")
	auth := smtp.PlainAuth("", share.GlobalContext.Email.Sender, share.GlobalContext.Email.Password, hp[0])

	err = smtp.SendMail(share.GlobalContext.Email.Server, auth, share.GlobalContext.Email.Sender, share.GlobalContext.Email.Receivers, data.Bytes())
	if err != nil {
		return err
	}

	return
}
