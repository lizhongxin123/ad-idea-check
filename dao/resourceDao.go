package dao

import (
	"ad-idea-check/model"
	"ad-idea-check/share"
	utils "ad-idea-check/util"
	"github.com/go-xorm/xorm"
	"time"
)

type ResourceDao struct {
	engine *xorm.Engine
}

func NewResourceDao() *ResourceDao {
	return &ResourceDao{
		engine: share.DevEngine,
	}
}

// FindDataListByIds 根据状态筛选
func (d *ResourceDao) FindDataListByIds(ids ...string) ([]model.Resource, error) {
	idList := make([]int, 0)
	for _, id := range ids {
		if id != "0" {
			idList = append(idList, utils.StrTo(id).MustInt())
		}
	}

	var resourceList []model.Resource
	err := d.engine.Table("resource").In("id ", idList).Find(&resourceList)
	if err != nil {
		return nil, err
	}
	return resourceList, nil
}

// UpdateStatusByIds 更新创意状态根据id
func (d *ResourceDao) UpdateStatusByIds(status, refuseReason, auditUserName string, auditTime time.Time, ids ...int) error {
	IdList := make([]int, 0)
	for _, id := range ids {
		IdList = append(IdList, id)
	}
	idea := model.Resource{
		Status:        status,
		RefuseReason:  refuseReason,
		AuditTime:     auditTime,
		AuditUserName: auditUserName,
	}
	_, err := d.engine.In("id", IdList).Cols("status", "refuse_reason", "audit_time", "audit_user_name").Update(&idea)
	return err
}
