package dao

import (
	"ad-idea-check/model"
	"ad-idea-check/share"
	utils "ad-idea-check/util"
	"github.com/go-xorm/xorm"
	"strings"
)

type IdeaDao struct {
	engine *xorm.Engine
}

func NewIdeaDao() *IdeaDao {
	return &IdeaDao{
		engine: share.DevEngine,
	}
}

/*
SELECT u.ideas AS ideas
 FROM plan p
 LEFT JOIN bill b ON p.id= b.plan_id
  AND b.date= CURDATE()
 LEFT JOIN user_extend e ON p.user_id= e.user_id
 INNER JOIN unit u ON u.plan_id= p.id
 INNER JOIN user us ON us.id= p.user_id
WHERE((b.fee+ b.coupon)< p.budget
   OR b.plan_id is NULL)
  AND(p.start_date<= CURDATE()
   OR p.start_date IS NULL)
  AND(p.end_date>= CURDATE()
   OR p.end_date IS NULL)
  AND u.status= 0
  AND p.status= 0
  AND us.status= 2
  AND u.ideas!= ''
  AND us.balance+ us.coupon> 0
  and us.id= 1000071  ---根据情况来改
u.ideas需要用“,”链接每行数据作为下一个sql的入参

a. 在投创意数，%s用u.ideas填充
SELECT count(*) cnt
 FROM idea i
 INNER JOIN resource r ON r.id in (i.image)
WHERE i.id in (%s)
AND i.status= 0
  AND i.type!= 6
  AND i.audit= 1
*/
// FindIdListByStatus 根据状态筛选出所有创意
func (d *IdeaDao) FindDataListByStatusAndAudit(status, audit int) ([]model.Idea, error) {
	DataList := make([]model.Idea, 0)
	sql1 := `SELECT u.ideas AS ideas
 FROM plan p
 LEFT JOIN bill b ON p.id= b.plan_id
  AND b.date= CURDATE()
 LEFT JOIN user_extend e ON p.user_id= e.user_id
 INNER JOIN unit u ON u.plan_id= p.id
 INNER JOIN user us ON us.id= p.user_id
WHERE((b.fee+ b.coupon)< p.budget
   OR b.plan_id is NULL)
  AND(p.start_date<= CURDATE()
   OR p.start_date IS NULL)
  AND(p.end_date>= CURDATE()
   OR p.end_date IS NULL)
  AND u.status= 0
  AND p.status= 0
  AND us.status= 2
  AND u.ideas!= ''
  AND us.balance+ us.coupon> 0`

	results, err := d.engine.QueryString(sql1)
	if err != nil {
		return DataList, err
	}
	ideaListId := make([]int, 0)
	for _, v := range results {
		ideasSplit := strings.Split(v["ideas"], ",")
		ideas := make([]int, 0)
		for _, id := range ideasSplit {
			ideas = append(ideas, utils.StrTo(id).MustInt())
		}
		ideaListId = append(ideaListId, ideas...)
	}

	err = d.engine.Table("idea").Where("idea.status = ? and idea.audit = ?  and idea.type != 6", status, audit).
		In("idea.id", ideaListId).Join("INNER", "resource", "resource.id in (idea.image)").
		Find(&DataList)
	if err != nil {
		return DataList, err
	}
	return DataList, nil
}

// UpdateStatusById 更新创意状态根据id
func (d *IdeaDao) UpdateStatusByIds(status, audit int, refuseType string, ids ...int) error {
	IdList := make([]int, 0)
	for _, id := range ids {
		IdList = append(IdList, id)
	}
	idea := model.Idea{
		Status:     status,
		Audit:      audit,
		RefuseType: refuseType,
	}
	_, err := d.engine.In("id", IdList).Cols("status", "audit", "refuse_type").Update(&idea)
	return err
}
