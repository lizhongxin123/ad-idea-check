package share

import (
	"ad-idea-check/model"
	"ad-idea-check/share/runtime"
	"bitbucket.org/hyrainbow/clog"
	"flag"
	"github.com/go-xorm/xorm"
	"log"
)

var (
	err        error
	configFile = flag.String("conf", "conf/prod.conf", "config file path")

	Env           = flag.String("env", "prod", "runtime environment")
	GlobalContext model.GlobalContext

	// MysqlEngine
	DevEngine *xorm.Engine
)

func InitShare() {
	//读取配置文件到GlobalContext
	InitGlobalContext()
	clogConf := &clog.Conf{
		Name:    GlobalContext.Log.Name,
		LogPath: GlobalContext.Log.LogPath,
		BufSize: map[clog.LogLevel]int{
			clog.NoticeLevel:  GlobalContext.Log.BufSize,
			clog.FatalLevel:   GlobalContext.Log.BufSize,
			clog.WarningLevel: GlobalContext.Log.BufSize,
			clog.DebugLevel:   GlobalContext.Log.BufSize},
		MaxEnableLevel: clog.LogLevel(GlobalContext.Log.MaxEnableLevel),
		NeedEmail:      GlobalContext.Log.NeedEmail,
		EmailConf: &clog.EmailConf{
			Server:    GlobalContext.Email.Server,
			Sender:    GlobalContext.Email.Sender,
			Password:  GlobalContext.Email.Password,
			Receivers: GlobalContext.Email.Receivers,
			Subject:   GlobalContext.Email.Subject,
			HostName:  "",
		},
	}

	// 初始化日志Clog
	runtime.CLog, err = clog.NewClog(clogConf)
	if err != nil {
		log.Fatalln("Init CLog", "Init CLog err: %v", err)
	}
	runtime.CLog.Debug("Sever", "load flag success")

	// 初始化Mysql
	InitMysql()
	runtime.CLog.Debug("Sever", "load DB success")
}
