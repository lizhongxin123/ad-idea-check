package share

import (
	"flag"
	"gopkg.in/gcfg.v1"
	"log"
)

func InitGlobalContext() {
	//读取配置文件
	flag.Parse()
	log.Println("Init Cong file: ", *configFile)
	if err := gcfg.ReadFileInto(&GlobalContext, *configFile); err != nil {
		log.Println("Init config", "Failed to read global conf err[%s]: %v", err)
		return
	}
}
