package share

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-xorm/xorm"
	"log"
	"time"
)

func InitMysql() {
	DevEngine, err = xorm.NewEngine(GlobalContext.Adv.DbType, fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8mb4&parseTime=True",
		GlobalContext.Adv.DbUser,
		GlobalContext.Adv.DbPassword,
		GlobalContext.Adv.DbHost,
		GlobalContext.Adv.DbName,
	))
	if err != nil {
		log.Fatalf("database NewEngine err: %s\n", err.Error())
	}
	if err != nil {
		log.Fatalf("database Sync2 err: %s\n", err.Error())
	}

	//Engine.TZLocation, _ = time.LoadLocation("Asia/Shanghai") //上海时区

	if err = DevEngine.Ping(); err != nil { //数据库 ping
		log.Fatalf("database ping err: %s\n", err.Error())
	}

	if *Env == "prod" { //根据运行环境判断日志模式
		DevEngine.ShowSQL(false)
	} else {
		DevEngine.ShowSQL(true)
	}

	//DevEngine.ShowSQL(true)

	DevEngine.SetMaxIdleConns(GlobalContext.Adv.DbMaxIdleConn) //连接池的空闲数大小
	DevEngine.SetMaxOpenConns(GlobalContext.Adv.DbMaxOpenConn) //最大打开连接数

	timer := time.NewTicker(time.Minute * 30) //定时器 ping
	go func(x *xorm.Engine) {
		for range timer.C {
			err = x.Ping()
			if err != nil {
				log.Fatalf("数据库连接错误: %#v\n", err.Error())
			}
		}
	}(DevEngine)
}
